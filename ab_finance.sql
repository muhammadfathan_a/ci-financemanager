-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 04, 2019 at 06:22 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ab_finance`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_categoryIn`
--

CREATE TABLE `t_categoryIn` (
  `id_categoryIn` int(11) NOT NULL,
  `cat_name_in` enum('GAJI','TEMAN','TAK TERDUGA','LEMBUR','ORANG TUA') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_categoryIn`
--

INSERT INTO `t_categoryIn` (`id_categoryIn`, `cat_name_in`) VALUES
(1, 'GAJI'),
(2, 'TEMAN'),
(3, 'TAK TERDUGA'),
(4, 'LEMBUR'),
(5, 'ORANG TUA');

-- --------------------------------------------------------

--
-- Table structure for table `t_categoryOut`
--

CREATE TABLE `t_categoryOut` (
  `id_categoryOut` int(11) NOT NULL,
  `cat_name_out` enum('LISTRIK','MAKAN','MINUM','BELANJA','UTANG','KULIAH','ORANG TUA','KOST','AIR') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_categoryOut`
--

INSERT INTO `t_categoryOut` (`id_categoryOut`, `cat_name_out`) VALUES
(1, 'LISTRIK'),
(2, 'MAKAN'),
(3, 'MINUM'),
(4, 'BELANJA'),
(5, 'UTANG'),
(6, 'KULIAH'),
(7, 'ORANG TUA'),
(8, 'KOST'),
(9, 'AIR');

-- --------------------------------------------------------

--
-- Table structure for table `t_feeIn`
--

CREATE TABLE `t_feeIn` (
  `num_in` varchar(255) NOT NULL,
  `time_in` time NOT NULL,
  `fee_in` varchar(11) NOT NULL,
  `id_categoryIn` int(11) NOT NULL,
  `information_in` text NOT NULL,
  `date_in` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_feeIn`
--

INSERT INTO `t_feeIn` (`num_in`, `time_in`, `fee_in`, `id_categoryIn`, `information_in`, `date_in`) VALUES
('5d3765fe7b0c4', '02:53:53', '25000', 2, 'Kesambet keknya dia', '2019-07-24'),
('5d385ff02f727', '20:39:23', '30000', 3, 'Nemu dijalan terbang', '2019-07-24'),
('5d38b55a8db51', '02:45:14', '1000000', 1, 'Dikirim dari langit', '2019-07-25'),
('5d3993bb3f554', '18:34:02', '500000', 3, 'Dari langit', '2019-07-25'),
('5d44b644dcab6', '05:16:23', '300000', 1, 'Website FT', '2019-08-03'),
('5d44b65b2a197', '05:16:45', '250000', 1, 'AsamBang Finance', '2019-08-03'),
('5d6a012baacd6', '12:09:13', '3000000', 1, 'Foya Foya skuy', '2019-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `t_feeOut`
--

CREATE TABLE `t_feeOut` (
  `num_out` varchar(255) NOT NULL,
  `time_out` time NOT NULL,
  `fee_out` varchar(11) NOT NULL,
  `id_categoryOut` int(11) NOT NULL,
  `information_out` text NOT NULL,
  `date_out` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_feeOut`
--

INSERT INTO `t_feeOut` (`num_out`, `time_out`, `fee_out`, `id_categoryOut`, `information_out`, `date_out`) VALUES
('5d376612a0c06', '02:54:38', '15000', 2, 'Akhirnya bisa makan wkwk', '2019-07-24'),
('5d386012c18e3', '20:41:04', '5000', 3, 'Aqua 1.5 Liter', '2019-07-24'),
('5d38b5438f5c3', '02:44:49', '100000', 4, 'Sedekah dimasjid :)', '2019-07-25'),
('5d3993d56a9da', '18:34:20', '15000', 2, 'Ayam geprek favorit', '2019-07-25'),
('5d6a013b652f4', '12:10:04', '15000', 2, 'Ayam Geprek', '2019-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `t_reports`
--

CREATE TABLE `t_reports` (
  `num_report` varchar(11) NOT NULL,
  `date` date NOT NULL,
  `saldo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_reports`
--

INSERT INTO `t_reports` (`num_report`, `date`, `saldo`) VALUES
('5d386030757', '2019-07-24', 35000),
('5d38b5621b0', '2019-07-25', 900000),
('5d6a01492b7', '2019-08-31', 2985000);

-- --------------------------------------------------------

--
-- Table structure for table `t_users`
--

CREATE TABLE `t_users` (
  `id_user` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(32) NOT NULL,
  `nm_user` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_users`
--

INSERT INTO `t_users` (`id_user`, `username`, `password`, `nm_user`) VALUES
('1d376612a0c01', 'financemanager', '21232f297a57a5a743894a0e4a801fc3', 'Superadmin'),
('5d3f652c07d23', 'akhtarfath', '0b3833d6b93aacdcbcad0a9a2b960df9', 'Muhammad A. Fathan'),
('5d3f6585c7f49', 'justirva', '21232f297a57a5a743894a0e4a801fc3', 'Muhammad Irva'),
('5d3f6d81105c2', 'justirva', '21232f297a57a5a743894a0e4a801fc3', 'Muhammad Irva');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_categoryIn`
--
ALTER TABLE `t_categoryIn`
  ADD PRIMARY KEY (`id_categoryIn`);

--
-- Indexes for table `t_categoryOut`
--
ALTER TABLE `t_categoryOut`
  ADD PRIMARY KEY (`id_categoryOut`);

--
-- Indexes for table `t_feeIn`
--
ALTER TABLE `t_feeIn`
  ADD PRIMARY KEY (`num_in`),
  ADD KEY `date_in` (`date_in`),
  ADD KEY `category_in` (`id_categoryIn`);

--
-- Indexes for table `t_feeOut`
--
ALTER TABLE `t_feeOut`
  ADD PRIMARY KEY (`num_out`),
  ADD KEY `category_out` (`id_categoryOut`),
  ADD KEY `date_out` (`date_out`);

--
-- Indexes for table `t_reports`
--
ALTER TABLE `t_reports`
  ADD PRIMARY KEY (`num_report`),
  ADD UNIQUE KEY `date` (`date`);

--
-- Indexes for table `t_users`
--
ALTER TABLE `t_users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_categoryIn`
--
ALTER TABLE `t_categoryIn`
  MODIFY `id_categoryIn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_categoryOut`
--
ALTER TABLE `t_categoryOut`
  MODIFY `id_categoryOut` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_feeIn`
--
ALTER TABLE `t_feeIn`
  ADD CONSTRAINT `t_feeIn_ibfk_1` FOREIGN KEY (`id_categoryIn`) REFERENCES `t_categoryIn` (`id_categoryIn`);

--
-- Constraints for table `t_feeOut`
--
ALTER TABLE `t_feeOut`
  ADD CONSTRAINT `t_feeOut_ibfk_1` FOREIGN KEY (`id_categoryOut`) REFERENCES `t_categoryOut` (`id_categoryOut`);

--
-- Constraints for table `t_reports`
--
ALTER TABLE `t_reports`
  ADD CONSTRAINT `t_reports_ibfk_1` FOREIGN KEY (`date`) REFERENCES `t_feeIn` (`date_in`),
  ADD CONSTRAINT `t_reports_ibfk_2` FOREIGN KEY (`date`) REFERENCES `t_feeOut` (`date_out`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
