<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reg extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');

		$this->load->model('ToRegister');
		
		$this->load->library('DayDate');
		
		if($this->session->userdata('status') == "login"){
			redirect(base_url("dashboard"));
		}
	}

	public function index()
	{
		$data["page_title"] = 'AsamBang | Register';
		
		// Libraries Day Date
		$data['day']		= $this->daydate->thisDay();
		$data['date']		= $this->daydate->thisDate(date('Y-m-d'));
		
		$this->load->view('libraries/header', $data);
		$this->load->view('reg/index');
		$this->load->view('libraries/footer');
	}

	public function action_register()
	{
		$ToRegister = $this->ToRegister; 
		$ToRegister -> save();
		$this->session->set_flashdata('success', 'Berhasil Di Simpan');	
		
		// echo '<script> window.location ="'. base_url() .'login"; </script>';
	}
}
