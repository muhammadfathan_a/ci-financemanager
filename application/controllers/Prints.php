<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prints extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');

		$this->load->model('FeeIn');
		$this->load->model('FeeOut');
        $this->load->model('Reports');
        
		$this->load->library('DayDate');
		
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}
	}

	public function index()
	{
        $data['page_title'] = "Asambang | Finance";
		
		// Models FeeIn and FeeOut
		$data['feeIn'] 		= $this->FeeIn->getAll();
		$data['feeOut'] 	= $this->FeeOut->getAll();

		// Libraries Day Date
		$data['day']		= $this->daydate->thisDay();
		$data['date']		= $this->daydate->thisDate(date('Y-m-d'));

		$this->load->view('libraries/header', $data);
		$this->load->view('prints/index', $data);
		$this->load->view('libraries/footer');
	}
}
