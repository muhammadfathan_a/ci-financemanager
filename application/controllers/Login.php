<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->model('ToLogin');

		$this->load->library('DayDate');
	}

	public function index()
	{
		$data["page_title"] = 'AsamBang | Login';

		// Libraries Day Date
		$data['day']		= $this->daydate->thisDay();
		$data['date']		= $this->daydate->thisDate(date('Y-m-d'));
		
		$this->load->view('libraries/header', $data);
		$this->load->view('login/index');
		$this->load->view('libraries/footer');
		
		if($this->session->userdata('status') == "login"){
			redirect(base_url("dashboard"));
		}
	}

	public function action_login()
	{
		$data["page_title"] = 'AsamBang | Login';

		$username = $this->input->post('username');
		$password = $this->input->post('password');
			
			$where = array(
				'username' => $username,
				'password' => md5($password)
			);

		$cek = $this->ToLogin->check_login('t_users', $where)->num_rows();

		if($cek > 0) 
		{
			$data_session = array(
				'username' => $username,
				'status' => "login",
			);

			$this->session->set_userdata($data_session);
			
			redirect(base_url('dashboard'));
		}
		else 
		{
			header("Location:".base_url('login?gagal'));
		}
	}

	public function action_logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('login/'));
	}

}
