<body>
	<div class="container login">
	<div class="card login mb-0">
		<div class="card-header pt-3 pb-5">
			<span style="position: absolute; left: 20px;">
				<b> Finance Manager </b>
				<br>
				<small> Buat akun baru </small>
			</span>
			<span style="position: absolute; right: 20px;">
				<br>
				<small><?= $day; ?>, <?= $date; ?></small>
			</span>
		</div>
		<div class="card-body">
		<form action="<?= base_url('reg/action_register'); ?>" method="post">
			<div class="form-group">
				<label for="exampleInputEmail1"> Nama Lengkap </label>
				<div class="input-group">
					<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required="required">
				</div>
			</div>
			<div class="form-group">
				<label for="exampleInputEmail1"> Username </label>
				<div class="input-group">
					<div class="input-group-prepend">
					<div class="input-group-text">@</div>
					</div>
					<input type="text" name="username" class="form-control" placeholder="username" required="required">
				</div>
				<small class="form-text text-muted">
					We'll never share your username with anyone else.
				</small>
			</div>
			<hr>
			<div class="form-group">
				<label for="exampleInputPassword1">Password</label>
				<input type="password" name="password" class="form-control" placeholder="password" required="required">
				<a href="<?= base_url('login'); ?>" class="pl-1"><small> Sudah punya akun? lakukan login </small></a>
			</div>
			<button type="submit" name="btn-masuk" class="btn btn-danger pt-1 pb-1"> Daftar </button>
		</form>	
		</div>
		<div class="card-footer">
			<p class="small pt-3 pb-4">
				<span style="position: absolute; left: 20px;">
					Copyright &copy; 2019 AsamBang IT Support 
				</span>
				<span style="position: absolute; right: 20px;">
					<a href="https://www.instagram.com/asambang_itsupport/" target="_blank">
                	    <i class="menu-icon fa fa-instagram ig" 
							style="font-size: 23px; position: relative; right: 10px; color: #ff0061;">
						</i>
                    </a>
				</span>
			</p>
		</div>
	</div>
	</div>
</body>
<style>
.card.login {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
}
.card .card-body {
    float: left;
    padding: 1.25em;
    position: relative;
    width: 100%;
    /* background-image: url(https://w.wallhaven.cc/full/vg/wallhaven-vg5pdp.jpg); */
    background-size: 45%;
    background-repeat: no-repeat;
    background-position-x: 725px;
    background-position-y: 285px;
    background-attachment: fixed;
}
/* .card-header.pt-3.pb-5 {
    padding-bottom: 65px !important;
    background: linear-gradient(125deg, #ff253a, #873bd2);
    color: #fff;
    margin-top: -1px;
}
.card-footer {
    background: #252525 !important;
    color: #fff !important;
	margin-bottom: -1px;
} */
.card-header.pt-3.pb-5 {
    padding-bottom: 65px !important;
    background: #fff;
    color: #8a8a8a;
    margin-top: -1px;
}
.card-footer {
    background: #fff !important;
    color: #484848 !important;
	margin-bottom: -1px;
}
</style>
