<?php error_reporting(1); ?>
<!-- Content -->
<div class="content">
    <!-- Animated -->
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="box-title"> Laporan </h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card-body">
                                <table style="width: 100%;" class="table table-hover" id="dataTable" cellspacing="0">
                                    <tr>
                                        <th class="field no" style="border-bottom: 1px solid #eee; text-align: left;"> no </th>
                                        <th class="field" style="border-bottom: 1px solid #eee; text-align: left;"> waktu </th>
                                        <th class="field" style="border-bottom: 1px solid #eee; text-align: right;"> saldo </th>
                                        <th class="field" style="border-bottom: 1px solid #eee; text-align: center;"> aksi </th>
                                    </tr>
                            <?php $no = 1; foreach($reports as $laporan): $jumLaporan[] = $laporan->saldo;?> 
                                    <tr>
                                        <td class="field no" style="background: #eee; background: #f7f7f7; text-align: center;"><?= $no; ?></td>
                                        <td class="field" style="text-transform: lowercase;">
                                            <a href="<?= base_url().'finance/?date='.$laporan->date; ?>">
                                                <?= $laporan->date; ?>
                                            </a>
                                            <br>
                                            <small>
                                                Klik tanggal untuk melihat riwayat keuangan
                                            </small>
                                        </td>
                                        <td class="field" style="text-align: right;">
                                            <?= number_format($laporan->saldo_total,2,',','.'); ?>
                                        </td>
                                        <td class="field" style="text-align: center;">
                                            <a href="<?= base_url().'report/delete/?num_report='.$laporan->num_report; ?>" class="btn btn-small text-danger"> HAPUS &times;</a>
                                        </td>
                                    </tr>
                            <?php $no++; $laporanIn = array_sum($jumLaporan); endforeach; ?>
                                </table>    
                            </div> <!-- /.card-body -->
                            <div class="card-body">                                
                                <table style="width: 100%; border-top: 1px solid #eee;">
                                    <tr>
                                    <?php if($laporanIn == null) { $laporanIn = 0; ?>
                                        <td class="field" style="border-bottom: 1px solid #eee;"><b> Belum Ada Laporan </b></td>
                                    <?php } else { ?>
                                        <td class="field" style="border-bottom: 1px solid #eee;"><b> Total Saldo </b></td>
                                        <td class="field" style="border-bottom: 1px solid #eee; border-top: 1px solid #eee; text-align: right;"><b><?= number_format($laporanIn,2,',','.'); ?></b></td>
                                        <td class="field" style="border-bottom: 1px solid #eee; border-top: 1px solid #eee; text-align: right;"><b></b></td>
                                    <?php } ?>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                    <div class="card-body"></div>
                </div>
            </div><!-- /# column -->
        </div>
        <!--  /Traffic -->
        <div class="clearfix"></div>
    <!-- /#add-category -->
    </div>
    <!-- .animated -->
</div>
<!-- /.content -->
<div class="clearfix"></div>

<style>
    .card-body.finance {
        height: 440px;
        overflow: auto;
        max-height: 355px;
    }
    .field {
        padding: 13px 17px;
    }
    .field.no {
        border-bottom: 1px solid #eee;
        width: 55px;
    }
    .card-body.data-finance {
        padding-top: 5px;
    }
    .form-group.btn {
        margin: 0px 0px;
        display: grid;
        padding: 13px 0px;
    }

    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 100% !important;
            margin: 1rem auto;
        }
        textarea#inputInformasiPemasukkan {
            height: 175px;
        }
        .card .card-body {
            float: left;
            padding: 1.25em;
            position: relative;
            width: 100%;
            /* background: #191919;
            color: #fff; */
        }
        .field.no {
            border-bottom: 1px solid #eee;
            width: 55px;
            /* color: #000; */
        }
        th.field.no.th {
            /* color: #000; */
        }
        a.btn.btn-small.text-danger {
            background: #d00000;
            color: #fff !important;
            font-weight: 900;
            font-size: 12px;
        }
        a.btn.btn-small.text-danger:hover {
            background: #940000;
            cursor: pointer;
        }
        .card {
            margin-bottom: 1.875em;
            border-radius: 5px;
            zoom: 85%;
            padding: 0;
            border: 0px solid transparent;
            -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, 0.08);
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.08);
        }
        .card-body.head-finance {
            /* background: #424242;
            box-shadow: 0px 3px 6px -5px black; */
        }
        th.field.th {
            /* background: #ffffff; */
            color: #000;
            border: none;
        }
        td.field.mb-1 {
            padding-bottom: 0px;
        }
        table.saldo {
            position: relative;
            top: 25px;
        }
        button.close {
            color: #fff;
            font-weight: 900;
        }
    }
</style>