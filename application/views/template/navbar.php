<!-- Header-->
<header id="header" class="header">
    <div class="top-left">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?= base_url(); ?>">
                <!-- <img src="<?= base_url(); ?>assets/images/logo.png" alt="Logo"> -->
                <h3> Finance <small class="h6"> Manager </small></h3>
            </a>
            <a class="navbar-brand hidden" href="<?= base_url(); ?>">
                <!-- <img src="<?= base_url(); ?>assets/images/logo2.png" alt="Logo">-->
            </a>
            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>
        </div>
    </div>
    <div class="top-right">
        <div class="header-menu">
            <div class="header-left">
                <!-- <button class="search-trigger"><i class="fa fa-search"></i></button>
                <div class="form-inline">
                    <form class="search-form">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                        <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                    </form>
                </div> -->

                <!-- <div class="dropdown for-notification">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                        <span class="count bg-danger">3</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="notification">
                        <p class="red"> Proses FinanceApp </p>
                        <a class="dropdown-item media" href="#">
                            <i class="fa fa-check"></i>
                            <p> #1 Tambah Data </p>
                        </a>
                        <a class="dropdown-item media" href="#">
                            <i class="fa fa-info"></i>
                            <p> #2 Hapus Data </p>
                        </a>
                        <a class="dropdown-item media" href="#">
                            <i class="fa fa-warning"></i>
                            <p> #3 Laporan Keuangan </p>
                        </a>
                    </div>
                </div> -->

                <!-- <div class="dropdown for-message">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="message" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-envelope"></i>
                        <span class="count bg-primary">4</span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="message">
                        <p class="red">You have 4 Mails</p>
                        <a class="dropdown-item media" href="#">
                            <span class="photo media-left"><img alt="avatar" src="<?= base_url(); ?>assets/images/avatar/1.jpg"></span>
                            <div class="message media-body">
                                <span class="name float-left">Jonathan Smith</span>
                                <span class="time float-right">Just now</span>
                                <p>Hello, this is an example msg</p>
                            </div>
                        </a>
                        <a class="dropdown-item media" href="#">
                            <span class="photo media-left"><img alt="avatar" src="<?= base_url(); ?>assets/images/avatar/2.jpg"></span>
                            <div class="message media-body">
                                <span class="name float-left">Jack Sanders</span>
                                <span class="time float-right">5 minutes ago</span>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </a>
                        <a class="dropdown-item media" href="#">
                            <span class="photo media-left"><img alt="avatar" src="<?= base_url(); ?>assets/images/avatar/3.jpg"></span>
                            <div class="message media-body">
                                <span class="name float-left">Cheryl Wheeler</span>
                                <span class="time float-right">10 minutes ago</span>
                                <p>Hello, this is an example msg</p>
                            </div>
                        </a>
                        <a class="dropdown-item media" href="#">
                            <span class="photo media-left"><img alt="avatar" src="<?= base_url(); ?>assets/images/avatar/4.jpg"></span>
                            <div class="message media-body">
                                <span class="name float-left">Rachel Santos</span>
                                <span class="time float-right">15 minutes ago</span>
                                <p>Lorem ipsum dolor sit amet, consectetur</p>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
            <div class="user-area dropdown float-right">
                <a href="#" class="dropdown-toggle active" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<span class="mr-3 hiName"> Hi, <?= $this->session->userdata('username'); ?></span>
					<div class="test"></div>
                    <!-- <img class="user-avatar rounded-circle" src="<?= base_url(); ?>assets/images/admin.jpg" alt="User Avatar"> -->
                </a>

                <div class="user-menu dropdown-menu">
                    <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i><?= $this->session->userdata('username'); ?></a> -->

                    <!-- <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                    <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a> -->

                    <a class="nav-link" href="<?= base_url('login/action_logout'); ?>"><i class="fa fa-power -off"></i>Logout</a>
                </div>
            </div>

        </div>
    </div>
</header>
<!-- /#header -->
<style>
    @media (min-width: 576px) {
        .right-panel header.header {
            /* background: linear-gradient(120deg, #004fc1, #00ff4c); */
			background: #fff;
            /* border-bottom: 3px solid #000000; */
            -webkit-box-shadow: none;
            box-shadow: none;
            clear: both;
            padding: 0 30px;
            height: 55px;
            position: fixed;
            left: 280px;
            left: 0;
            right: 0;
            top: 0;
            z-index: 999;
        }
		span.mr-3.hiName {
			color: #676767;
		}
        .right-panel .navbar-header {
            width: 100%;
            background-color: transparent;
            padding: 0 1.25em 0 0;
            color: #000;
        }
        .right-panel .navbar-header > a {
			display: inline-block;
			color: #676767;
			padding: 10px 0px;
		}
        .fa {
            display: inline-block;
            font: normal normal normal 14px/1 FontAwesome;
            font-size: inherit;
            text-rendering: auto;
            color: #75746f;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }
        .navbar {
            /* background: #191919; */
            border-radius: 0;
            border: none;
            display: inline-block;
            margin: 0;
            padding: 0;
            padding-top: 25px;
            /* color: #fff !important; */
            vertical-align: top;
        }
        aside.left-panel {
            /* background: #191919; */
            height: 100vh;
            padding: 0;
            vertical-align: top;
            width: 280px;
            -webkit-box-shadow: 1px 0 20px rgba(0, 0, 0, 0.08);
            box-shadow: 1px 0 20px rgba(0, 0, 0, 0.08);
            position: fixed;
            left: 0;
            bottom: 0;
            top: 55px;
            z-index: 999;
        }
        .navbar .navbar-nav li > a {
            background: none !important;
            /* color: #9c9c9c; */
            display: inline-block;
            font-size: 14px;
            line-height: 26px;
            padding: 10px 0;
            position: relative;
            width: 100%;
        }
        .navbar .menu-title {
            /* color: #f1f1f1; */
            clear: both;
            display: block;
            font-family: 'Open Sans';
            font-size: 14px;
            font-weight: 700;
            line-height: 50px;
            padding: 0;
            text-transform: uppercase;
            width: 100%;
        }
		.test:after {
			content: '\2807';
			font-size: 1.3em;
			color: #2e2e2e
		}
		.user-area .dropdown-toggle.active:before {
			display: none;
		}
		.user-area .user-menu {
			box-shadow: 5px 5px 3px rgba(0, 0, 0, 0.07);
		}
    }
    @media (max-width: 768px) {
        aside.left-panel {
            overflow: scroll;
            position: relative;
            height: 210px !important;
            margin-top: -15px;
            padding-top: 10px;
        }
        .navbar {
            height: 0px !important;
        }
        .navbar .main-menu {
            float: none;
            padding-bottom: 0px !important;
        }
        .modal-dialog {
            position: relative;
            width: auto;
            margin: 0rem !important;
            pointer-events: none;
        }
		.test:after {
			content: '\2807';
			font-size: 1.3em;
			color: #2e2e2e;
			margin-left: 30px;
			margin-right: -35px;
		}
		.user-area .dropdown-toggle.active:before {
			display: none;
		}
		.user-area .user-menu {
			background: #fff;
			border: none;
			left: inherit !important;
			right: -60px;
			top: 54px !important;
			margin: 0;
			max-width: 150px;
			padding: 5px 10px;
			width: 100%;
			z-index: 999;
			min-width: 150px;
			box-shadow: 5px 5px 3px rgba(0, 0, 0, 0.07);
		}
    }
}
</style>
