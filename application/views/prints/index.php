<?php error_reporting(1); 

    $dns  = "mysql:host=127.0.0.1; dbname=ab_finance";
    $user = "root";
    $pass = "";

    try {

        $conn = new PDO($dns, $user, $pass);
        // echo "success connect!";
    }
    catch(PDOException $e) {
        echo "Connection Failed".$e->getMessage();
    }

?>
    <div class="animated fadeIn">
        <!--  Traffic  -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <?php if($_GET['date']) 
                        {
                            $tanggal_in  = $_GET['date'];
                            $tanggal_out = $_GET['date'];

                                $dataIn[] = $tanggal_in;
                                
                                $query   = 'SELECT DISTINCT * FROM t_feeIn
                                            INNER JOIN t_categoryIn
                                            ON t_feeIn.id_categoryIn = t_categoryIn.id_categoryIn
                                            WHERE t_feeIn.date_in = ?';
    
                                $row     = $conn -> prepare($query);
                                $row    -> execute($dataIn); 
                                $resultIn  = $row->fetchAll();

                                $dataOut[] = $tanggal_out;

                                $query   = 'SELECT DISTINCT * FROM t_feeOut
                                            INNER JOIN t_categoryOut
                                            ON t_feeOut.id_categoryOut = t_categoryOut.id_categoryOut
                                            WHERE t_feeOut.date_out = ?';

                                $row     = $conn -> prepare($query);
                                $row    -> execute($dataOut); 
                                $resultOut  = $row->fetchAll();
                    ?>
                    <div class="row">
                        <div class="col-lg-12 mt-3">
                            <b>
                            <span style="text-align: right; position: absolute; left: 20px;">
                                FinanceApp - AsamBangIT
                                <p style="text-align: right; position: absolute; left: 0px;"> 
                                    Millennial's IT Support 
                                </p>
                            </span>
                            <span style="text-align: right; position: absolute; right: 20px;" class="pr-3">
                                <?= $tanggal_in; ?>
                            </span>
                            </b>
                            <div class="card-body finance pt-5">
                                <div class="card-body data-finance">
                                    <table style="width: 100%;" class="table table-hover" id="dataTable" cellspacing="0">
                                        <tr>
                                            <th class="field no th" style="border-bottom: 1px solid #eee; text-align: left;"> no </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: left;"> waktu </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: left;"> penggunaan </th>
                                            <th class="field pemasukkan th" style="border-bottom: 1px solid #eee; text-align: left;"> keterangan </td>
                                            <th class="field th pager" style="border-bottom: 1px solid #eee; text-align: center;"> # </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: right;"> harga </th>
                                        </tr>
                                <?php $no = 1; foreach($resultIn as $hasil): $status = "( + )"; $In[] = $hasil['fee_in']; ?>
                                        <tr>
                                            <td class="field no" style="background: #eee; background: #f7f7f7; text-align: center;">
                                                <?= $no; ?>
                                            </td>
                                            <td class="field" style="text-transform: lowercase;"><?= $hasil['time_in']; ?></td>
                                            <td class="field" style="text-transform: lowercase;"><?= $hasil['cat_name_in']; ?></td>
                                            <td class="field" style="text-transform: lowercase;"><?= $hasil['information_in']; ?></td>
                                            <td class="field" style="text-align: center;"><?= $status; ?></td>
                                            <td class="field" style="text-align: right;">
                                                <?= number_format($hasil['fee_in'],2,',','.'); ?>
                                            </td>
                                        </tr>
                                <?php $no++; $feeIn = array_sum($In); endforeach; ?>
                                <tr class="tot">
                                <?php if($feeIn == null) { $feeIn = 0; ?>

                                <?php } else { ?>
                                    <td class="field mb-1" colspan="5" style="border-bottom: 1px solid #eee;">
                                        <b> Total Pemasukkan </b>
                                    </td>
                                    <td class="field" style="border-bottom: 1px solid #eee; border-top: 1px solid #eee; text-align: right;">
                                        <b> <?= number_format($feeIn,2,',','.'); ?> </b>
                                    </td>
                                <?php } ?>
                                </tr>
                                </table>
                                <table style="width: 100%;" class="table table-hover" id="dataTable" cellspacing="0">
                                        <tr>
                                            <th class="field no th" style="border-bottom: 1px solid #eee; text-align: left;"> no </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: left;"> waktu </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: left;"> penggunaan </th>
                                            <th class="field pemasukkan th" style="border-bottom: 1px solid #eee; text-align: left;"> keterangan </td>
                                            <th class="field th pager" style="border-bottom: 1px solid #eee; text-align: center;"> 
                                                # 
                                            </th>
                                            <th class="field th" style="border-bottom: 1px solid #eee; text-align: right;"> harga </th>
                                        </tr>
                            <?php $no = 1; foreach($resultOut as $isi): $status = "( - )"; $Out[] = $isi['fee_out']; ?>
                                        <tr>
                                            <td class="field no" style="background: #eee; background: #f7f7f7; text-align: center;">
                                                <?= $no; ?>
                                            </td>
                                            <td class="field" style="text-transform: lowercase;"><?= $isi['time_out']; ?></td>
                                            <td class="field" style="text-transform: lowercase;"><?= $isi['cat_name_out']; ?></td>
                                            <td class="field" style="text-transform: lowercase;"><?= $isi['information_out']; ?></td>
                                            <td class="field" style="text-align: center;"><?= $status; ?></td>
                                            <td class="field" style="text-align: right;">
                                                <?= number_format($isi['fee_out'],2,',','.'); ?>
                                            </td>
                                        </tr>
                            <?php $no++; $feeOut = array_sum($Out); endforeach; ?>
                                <tr class="tot">
                                <?php if($feeOut == null) { $feeOut = 0; ?>

                                <?php } else { ?>
                                    <td class="field mb-1" colspan="5" style="border-bottom: 1px solid #eee;"><b> Total Pengeluaran </b></td>
                                    <td class="field" style="border-bottom: 1px solid #eee; border-top: 1px solid #eee; text-align: right;"><b><?= number_format($feeOut,2,',','.'); ?></b></td>
                                <?php } ?>
                                </tr>
                                </table>
                            </div>
                        </div> <!-- /.card-body -->
                        <div class="card-body">                                
                            <table style="width: 100%; border-top: 1px solid #eee;" class="saldo">
                                <tr>

                                <?php if($feeIn == null && $feeOut == null) { $feeIn = 0; $feeOut = 0; ?> 

                                    <td class="field" colspan="2">
                                        <b> Saldo </b>
                                    </td>
                                    
                                    <?php $total = ($feeIn - $feeOut); $masuk->fee_in = $total; ?>
                                    
                                    <td class="field" style="text-align: right;" colspan="3">
                                        <b><?= number_format($total,2,',','.'); ?></b>
                                    </td>

                                <?php } else { ?>

                                    <td class="field" colspan="2">
                                        <b>
                                            <?= number_format($feeIn,2,',','.'); ?> 
                                                - 
                                            <?= number_format($feeOut,2,',','.'); ?> 
                                        </b>
                                    </td>

                                <?php $total = ($feeIn - $feeOut); $masuk->fee_in = $total; ?>
                                    
                                    <td class="field" style="text-align: right;
                                        border-top: 1px solid #eee;
                                        border-bottom: 1px solid #eee;" colspan="3">
                                        <b>Saldo, <?= number_format($total,2,',','.'); ?></b>
                                    </td>

                                <?php } ?>
                                </tr>
                                <?php  } else { ?>
                                
                                
                                <?php } ?>
<style>
    * {
        zoom: 98%;
    }
    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-top: 10px;
        margin-right: 0px;
        margin-left: 0px;
    }
</style>
<script>
    var css = '@page { size: landscape; }',
        head = document.head || document.getElementsByTagName('head')[0],
        style = document.createElement('style');

    style.type = 'text/css';
    style.media = 'print';

    if (style.styleSheet){
    style.styleSheet.cssText = css;
    } else {
    style.appendChild(document.createTextNode(css));
    }

    head.appendChild(style);

    window.print();
</script>
