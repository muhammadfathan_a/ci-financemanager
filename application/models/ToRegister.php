<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ToRegister extends CI_Model
{
    private $tableName = "t_users";

	function save()
	{		
		$post = $this->input->post();
            
		$this->id_user	= uniqid();
		$this->nm_user  = $post["nama"];
		$this->username = $post["username"];
		$this->password = md5($post["password"]);

		$this->db->insert($this->tableName, $this);

		redirect(base_url('login?daftar_sukses'));
	}	
}
